# ORS Web (External Interface) Sample #

A Composer project (SCXML application) and a NodeJS script to demonstrate the use of the Orchestration External Interface to allow two-way communication (over HTTP) between Orchestration Server and a 3rd party application.

### How do I get set up? ###

* Check out the Articles section of the Genesys DevFoundry (https://developer.genesys.com)

### To report issues or ask questions? ###

* Post to the Questions section of the Genesys DevFoundry (https://developer.genesys.com)
