//
// File: my3rdPartyApplicationSimulator.js
// Version 1.0.1
//

var http = require('http');
var dispatcher = require('httpdispatcher');
var url = require('url');
var request = require('request');

var ors = require('./basicORSExternalInterface');

const CmdArg_ORS_URL = 'ors_url';
const CmdArg_ORS_SessionID = 'id';
const CmdArg_Destination = 'dest';
const CmdArg_Message = 'msg';
const CmdArg_ReceivedData = 'data';

const CmdArgKey_ORS_URL = 'ors_url=';
const CmdArgKey_ORS_SessionID = 'id=';
const CmdArgKey_Destination = 'dest=';
const CmdArgKey_Message = 'msg=';

var listeningPort = 10230;

var current_ors_url = 'http://192.168.10.91:9076';
var current_session_id = '12345678';
var current_destination = '7001';
var current_message = '1200310';
var last_received_data = '';

// Utils
// Extracts parameters from command line and store them in data object/structure
// Ex: ors basic ors_url=http://192.168.10.91:9076, show ors_url, set ors_url=http://192.168.10.91:9076, ...
function getParamatersFromCmdArgs(input, startIndex) {
	
	var cmdArgs = input.split(" ");
	
	var paramsStructure = new Object();
	
	cmdArgs.forEach(
		function (element, index, array) {
			// startIndex == 1 for set/who, startIndex == 2 for ors basic/ors cfg/....
			if (index >= startIndex && element.trim() != '')
			{
				var tmpCmdArg = element.trim();
				
				if (tmpCmdArg == CmdArg_ORS_URL)
				{
					paramsStructure.ors_url_key = true;
				}
				else if (tmpCmdArg == CmdArg_ORS_SessionID)
				{
					paramsStructure.session_id_key = true;
				}
				else if (tmpCmdArg == CmdArg_Destination)
				{
					paramsStructure.destination_key = true;
				}
				else if (tmpCmdArg == CmdArg_Message)
				{
					paramsStructure.message_key = true;
				}
				else if (tmpCmdArg == CmdArg_ReceivedData)
				{
					paramsStructure.received_data_key = true;
				}
				else if (tmpCmdArg.startsWith(CmdArgKey_ORS_URL))
				{
					var tmpCmdArgValue = tmpCmdArg.substr(CmdArgKey_ORS_URL.length);
					paramsStructure.ors_url = tmpCmdArgValue;
				}
				else if (tmpCmdArg.startsWith(CmdArgKey_ORS_SessionID))
				{
					var tmpCmdArgValue = tmpCmdArg.substr(CmdArgKey_ORS_SessionID.length);
					paramsStructure.session_id = tmpCmdArgValue;
				}
				else if (tmpCmdArg.startsWith(CmdArgKey_Destination))
				{
					var tmpCmdArgValue = tmpCmdArg.substr(CmdArgKey_Destination.length);
					paramsStructure.destination = tmpCmdArgValue;
				}
				else if (tmpCmdArg.startsWith(CmdArgKey_Message))
				{
					var tmpCmdArgValue = tmpCmdArg.substr(CmdArgKey_Message.length);
					paramsStructure.message = tmpCmdArgValue;
				}
				
			}
		}
	);
	
	return paramsStructure;
	
};

// Utils
// Emulates prompt question/answer (cmdargs)
function prompt(question, callback) {
    var stdin = process.stdin,
        stdout = process.stdout;

    stdin.resume();
    stdout.write(question);

    stdin.once('data', function (data) {
        callback(data.toString().trim());
    });
};



// Start

// Process command line parameters
process.argv.forEach((val, index) => {
	// Override HTTP Server listening port: p=10234
	if (val.startsWith('p=')) {
		var portAsArg = parseInt(val.substr(2));
		if (isNaN(portAsArg))
		{
			console.log('Warning: Invalid Port argument format');
		} else {
			listeningPort = portAsArg;
			
			console.log('HTTP Server port has been set to: ' + listeningPort);
		}
	}
});


//define the dispatcher
function handleRequest(request, response){
    try {
        dispatcher.dispatch(request, response);
    } catch(err) {
		console.log("Error - HTTP server - handleRequest");
        console.log(err);
    }
};

//the POST request to start the service
dispatcher.onPost("/startService", function(req, res) {
	
	console.log("3rdPartyApp received startService request from ORS - with Content:");
	console.log(req.body);
	var page = url.parse(req.url).pathname;
	
	var receivedPOSTRequestData = JSON.parse(req.body);
	if (receivedPOSTRequestData.session_id != undefined)
		current_session_id = receivedPOSTRequestData['session_id'];
	if (receivedPOSTRequestData.ors_url != undefined)
		current_ors_url = receivedPOSTRequestData['ors_url'];
	last_received_data = receivedPOSTRequestData;
	
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end("{result = 'ok'}");
});


// Cmd Line Interface
function getNextCmd() {
    
	prompt('CMD>', function (input) {
		var matchedString = '';
		
		if ((input == '?') || (input == 'h') || (input == 'help'))
		{
			console.log("Help - Available commands:");
			console.log("     " + "* Help: ?, h, help");
			console.log("");
			console.log("     " + "* Set variables: set ors_url=[url] id=[sessionid] dest=[transfer destination] msg=[announcement ID]");
			console.log("     " + "* Show variables: show ors_url id dest msg data");
			console.log("");
			console.log("     " + "* ORS Get Basic Data: ors basic (opt: ors_url=[url])");
			console.log("     " + "* ORS Get Configuration Data: ors cfg (opt: ors_url=[url])");
			console.log("     " + "* ORS Get Interaction Data: ors ixn (opt: ors_url=[url])");
			console.log("     " + "* ORS Get SCXML Engine Data: ors scxml (opt: ors_url=[url])");
			console.log("     " + "* ORS Get SCXML Applications Data: ors app (opt: ors_url=[url])");
			console.log("");
			console.log("     " + "* ORS SCXML Query: query (opt: id=[session_id] ors_url=[url])");
			console.log("");
			console.log("     " + "* ORS SCXML SendRequest reqGetQueuingStatus: queuing (opt: id=[session_id] ors_url=[url])");
			console.log("     " + "* ORS SCXML SendRequest reqUndeclaredRequest: badrequest (opt: id=[session_id] ors_url=[url])");
			console.log("");
			console.log("     " + "* ORS SCXML PublishEvent evtInterruptAndTransfer: transfer dest=1234 (opt: id=[session_id] ors_url=[url])");
			console.log("     " + "* ORS SCXML PublishEvent evtPlayMessage: play msg=1234 (opt: id=[session_id] ors_url=[url])");
			console.log("     " + "* ORS SCXML PublishEvent evtUndeclaredEvent: badevent (opt: id=[session_id] ors_url=[url])");
			console.log("");
				
		}
		
		else if (input.startsWith('set'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			if (cmdArgParameters.ors_url != undefined)
			{
				current_ors_url = cmdArgParameters.ors_url;
				console.log("ORS Current URL has been set to: " + current_ors_url);
			}
			if (cmdArgParameters.session_id != undefined)
			{
				current_session_id = cmdArgParameters.session_id;
				console.log("ORS Current SessionID has been set to: " + current_session_id);
			}
			if (cmdArgParameters.destination != undefined)
			{
				current_destination = cmdArgParameters.destination;
				console.log("Current Destination has been set to: " + current_destination);
			}
			if (cmdArgParameters.message != undefined)
			{
				current_message = cmdArgParameters.message;
				console.log("Current Message has been set to: " + current_message);
			}
		}
		else if (input.startsWith('show'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			if (cmdArgParameters.ors_url_key != undefined)
			{
				console.log("ORS Current URL: " + current_ors_url);
			}
			if (cmdArgParameters.session_id_key != undefined)
			{
				console.log("ORS Current SessionID: " + current_session_id);
			}
			if (cmdArgParameters.destination_key != undefined)
			{
				console.log("Current Destination: " + current_destination);
			}
			if (cmdArgParameters.message_key != undefined)
			{
				console.log("Current Message: " + current_message);
			}
			if (cmdArgParameters.received_data_key != undefined)
			{
				console.log("Last Received Data: " + JSON.stringify(last_received_data));
			}
		}
		
		else if (input.startsWith('ors basic'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 2);
			var param_ors_url = current_ors_url;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			
			ors.serverGetBasicData(param_ors_url, function (orsResponse) {
				if (orsResponse.success)
				{
					last_received_data = orsResponse.content;
					console.log("ORS Get Basic Data Success. Received Content:");
					console.log(last_received_data);
				} else {
					console.log("ORS Get Basic Data Failed with status code: " + orsResponse.statusCode);
				}
			});
			
		}
		else if (input.startsWith('ors cfg'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 2);
			var param_ors_url = current_ors_url;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			
			ors.serverGetConfigurationData(param_ors_url, function (orsResponse) {
				if (orsResponse.success)
				{
					last_received_data = orsResponse.content;
					console.log("ORS Get Configuration Data Success. Received Content:");
					console.log(last_received_data);
				} else {
					console.log("ORS Get Configuration Data Failed with status code: " + orsResponse.statusCode);
				}
			});
		}
		else if (input.startsWith('ors ixn'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 2);
			var param_ors_url = current_ors_url;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			
			ors.serverGetInteractionData(param_ors_url, function (orsResponse) {
				if (orsResponse.success)
				{
					last_received_data = orsResponse.content;
					console.log("ORS Get Interaction Data Success. Received Content:");
					console.log(last_received_data);
				} else {
					console.log("ORS Get Interaction Data Failed with status code: " + orsResponse.statusCode);
				}
			});
		}
		else if (input.startsWith('ors scxml'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 2);
			var param_ors_url = current_ors_url;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			
			ors.serverGetSCXMLEngineData(param_ors_url, function (orsResponse) {
				if (orsResponse.success)
				{
					last_received_data = orsResponse.content;
					console.log("ORS Get SCXML Engine Data Success. Received Content:");
					console.log(last_received_data);
				} else {
					console.log("ORS Get SCXML Engine Data Failed with status code: " + orsResponse.statusCode);
				}
			});
		}
		else if (input.startsWith('ors app'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 2);
			var param_ors_url = current_ors_url;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			
			ors.serverGetSCXMLApplicationData(param_ors_url, function (orsResponse) {
				if (orsResponse.success)
				{
					last_received_data = orsResponse.content;
					console.log("ORS Get SCXML Applications Data Success. Received Content:");
					console.log(last_received_data);
				} else {
					console.log("ORS Get SCXML Applications Data Failed with status code: " + orsResponse.statusCode);
				}
			});
		}
		
		else if (input.startsWith('query'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			var param_ors_url = current_ors_url;
			var param_session_id = current_session_id;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			if (cmdArgParameters.session_id != undefined)
			{
				param_session_id = cmdArgParameters.session_id;
			}
			
			if (param_ors_url != '' && param_session_id != '')
			{
				ors.scxmlSessionQuery(param_ors_url, param_session_id, function (orsResponse) {
					if (orsResponse.success)
					{
						last_received_data = orsResponse.content;
						console.log("ORS SCXML Query Success. Received Content:");
						//console.log(JSON.stringify(last_received_data));
						console.log(last_received_data);
					} else {
						console.log("ORS SCXML Query Failed with status code: " + orsResponse.statusCode);
					}
				});
			} else {
				console.log("ORS SCXML Query Error - empty parameters");
			}
			
		}
		else if (input.startsWith('queuing'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			var param_ors_url = current_ors_url;
			var param_session_id = current_session_id;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			if (cmdArgParameters.session_id != undefined)
			{
				param_session_id = cmdArgParameters.session_id;
			}
			
			if (param_ors_url != '' && param_session_id != '')
			{
				ors.scxmlSessionSendRequest(param_ors_url, param_session_id, 'reqGetQueuingStatus', { json: { some_key: 'some_value' } }, function (orsResponse) {
					if (orsResponse.success)
					{
						last_received_data = orsResponse.content;
						console.log("ORS SCXML Request reqGetQueuingStatus Success. Received Content:");
						//console.log(JSON.stringify(last_received_data));
						console.log(last_received_data);
					} else {
						console.log("ORS SCXML Request reqGetQueuingStatus Failed with status code: " + orsResponse.statusCode);
					}
				});
			} else {
				console.log("ORS SCXML Request Error - empty parameters");
			}
		}
		else if (input.startsWith('badrequest'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			var param_ors_url = current_ors_url;
			var param_session_id = current_session_id;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			if (cmdArgParameters.session_id != undefined)
			{
				param_session_id = cmdArgParameters.session_id;
			}
			
			if (param_ors_url != '' && param_session_id != '')
			{
				ors.scxmlSessionSendRequest(param_ors_url, param_session_id, 'reqUndeclaredRequest', { json: { some_key: 'some_value' } }, function (orsResponse) {
					if (orsResponse.success)
					{
						last_received_data = orsResponse.content;
						console.log("ORS SCXML Request reqUndeclaredRequest Success. Received Content:");
						//console.log(JSON.stringify(last_received_data));
						console.log(last_received_data);
					} else {
						console.log("ORS SCXML Request reqUndeclaredRequest Failed with status code: " + orsResponse.statusCode);
					}
				});
			} else {
				console.log("ORS SCXML Request Error - empty parameters");
			}
		}
		else if (input.startsWith('transfer'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			var param_ors_url = current_ors_url;
			var param_session_id = current_session_id;
			var param_destination = current_destination;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			if (cmdArgParameters.session_id != undefined)
			{
				param_session_id = cmdArgParameters.session_id;
			}
			if (cmdArgParameters.destination != undefined)
			{
				param_destination = cmdArgParameters.destination;
			}
			
			if (param_ors_url != '' && param_session_id != '' && param_destination != '')
			{
				ors.scxmlSessionPublishEvent(param_ors_url, param_session_id, 'evtInterruptAndTransfer', { json: { destination: param_destination } }, function (orsResponse) {
					if (orsResponse.success)
					{
						last_received_data = orsResponse.content;
						console.log("ORS SCXML Event evtInterruptAndTransfer Success. Received Content:");
						console.log(last_received_data);
					} else {
						console.log("ORS SCXML Event evtInterruptAndTransfer Failed with status code: " + orsResponse.statusCode);
					}
				});
			} else {
				console.log("ORS SCXML Event Error - empty parameters");
			}
		}
		else if (input.startsWith('play'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			var param_ors_url = current_ors_url;
			var param_session_id = current_session_id;
			var param_message = current_message;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			if (cmdArgParameters.session_id != undefined)
			{
				param_session_id = cmdArgParameters.session_id;
			}
			if (cmdArgParameters.message != undefined)
			{
				param_message = cmdArgParameters.message;
			}
			
			if (param_ors_url != '' && param_session_id != '' && param_message != '')
			{
				ors.scxmlSessionPublishEvent(param_ors_url, param_session_id, 'evtPlayMessage', { json: { message: param_message } }, function (orsResponse) {
					if (orsResponse.success)
					{
						last_received_data = orsResponse.content;
						console.log("ORS SCXML Event evtPlayMessage Success. Received Content:");
						console.log(last_received_data);
					} else {
						console.log("ORS SCXML Event evtPlayMessage Failed with status code: " + orsResponse.statusCode);
					}
				});
			} else {
				console.log("Error - empty parameters");
			}
		}
		else if (input.startsWith('badevent'))
		{
			var cmdArgParameters = getParamatersFromCmdArgs(input, 1);
			
			var param_ors_url = current_ors_url;
			var param_session_id = current_session_id;
			
			if (cmdArgParameters.ors_url != undefined)
			{
				param_ors_url = cmdArgParameters.ors_url;
			}
			if (cmdArgParameters.session_id != undefined)
			{
				param_session_id = cmdArgParameters.session_id;
			}
			
			if (param_ors_url != '' && param_session_id != '')
			{
				ors.scxmlSessionPublishEvent(param_ors_url, param_session_id, 'evtUndeclaredEvent', { json: { some_key: 'some_value' } }, function (orsResponse) {
					if (orsResponse.success)
					{
						last_received_data = orsResponse.content;
						console.log("ORS SCXML Event evtUndeclaredEvent Success. Received Content:");
						console.log(last_received_data);
					} else {
						console.log("ORS SCXML Event evtUndeclaredEvent Failed with status code: " + orsResponse.statusCode);
					}
				});
			} else {
				console.log("ORS SCXML Event Error - empty parameters");
			}
		}
        getNextCmd();
	});
};


//Create the http server
var server = http.createServer(handleRequest);

//And start the http server
server.listen(listeningPort, function(){
    console.log("3rdPartyApplication listening on: http://localhost:%s", listeningPort);
	getNextCmd();
	
});



