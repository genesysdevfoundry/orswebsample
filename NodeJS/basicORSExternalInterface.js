//
// File: basicORSExternalInterface.js
// Version 1.0.1
//

var request = require('request');

// Utils
// Send HTTP GET or POST
var send_HTTP_POST_With_JSONContent = function(full_url, jsonInput, callback) {
	
	var jsonRequestContent;
	if (jsonInput != undefined)
		jsonRequestContent = jsonInput;
	else
		jsonRequestContent = { json: { some_key: 'some_value' } };
	
	request.post(
		full_url,
		jsonRequestContent,
		function (error, response, body) {
			var requestResult = new Object();
			if (!error && response.statusCode == 200) {
				requestResult.success = true;
				requestResult.statusCode = response.statusCode;
				if (body)
					requestResult.content = body;
				else
					requestResult.content = '';
			} else {
				requestResult.success = false;
				if (response)
					requestResult.statusCode = response.statusCode;
				else
					requestResult.statusCode = 0;
				if (body)
					requestResult.content = body;
				else
					requestResult.content = '';
			}
			callback(requestResult);
		}
	);
}

var send_HTTP_GET_With_JSONContent = function(full_url, jsonInput, callback) {
	
	var jsonRequestContent;
	if (jsonInput != undefined)
		jsonRequestContent = jsonInput;
	else
		jsonRequestContent = { json: { some_key: 'some_value' } };
	
	request.get(
		full_url,
		jsonRequestContent,
		function (error, response, body) {
			
			var requestResult = new Object();
			
			if (!error && response.statusCode == 200) {
				requestResult.success = true;
				requestResult.statusCode = response.statusCode;
				if (body)
					requestResult.content = body;
				else
					requestResult.content = '';
			} else {
				requestResult.success = false;
				if (response)
					requestResult.statusCode = response.statusCode;
				else
					requestResult.statusCode = 0;
				if (body)
					requestResult.content = body;
				else
					requestResult.content = '';
			}
			callback(requestResult);
		}
	);
}

// Orchestration External Interface

var scxmlSessionPublishEvent = function(ors_url, session_id, event_name, jsonInput, callback) {
	
	var requestUrl = ors_url + '/scxml/session/' + session_id + '/event/' + event_name;
    
	send_HTTP_POST_With_JSONContent(requestUrl, jsonInput, callback);
	
}

var scxmlSessionSendRequest = function(ors_url, session_id, request_name, jsonInput, callback) {
	
	var requestUrl = ors_url + '/scxml/session/' + session_id + '/request/' + request_name;
    
	send_HTTP_POST_With_JSONContent(requestUrl, jsonInput, callback);
	
}

var scxmlSessionQuery = function(ors_url, session_id, callback) {
	
	var requestUrl = ors_url + '/scxml/session/' + session_id + '/query';
	
	send_HTTP_GET_With_JSONContent(requestUrl, { json: { } }, callback);
	
}

// ORS Server level

var serverGetBasicData = function(ors_url, callback) {
	
	var requestUrl = ors_url;
	
	send_HTTP_GET_With_JSONContent(requestUrl, {}, callback);
	
}

var serverGetConfigurationData = function(ors_url, callback) {
	
	var requestUrl = ors_url + '/server?cfgStatistics';
	
	send_HTTP_GET_With_JSONContent(requestUrl, {}, callback);
	
}

var serverGetInteractionData = function(ors_url, callback) {
	
	var requestUrl = ors_url + '/server?activeCalls';
	
	send_HTTP_GET_With_JSONContent(requestUrl, {}, callback);
	
}

var serverGetSCXMLEngineData = function(ors_url, callback) {
	
	var requestUrl = ors_url + '/server?scxmlStat';
	
	send_HTTP_GET_With_JSONContent(requestUrl, {}, callback);
	
}

var serverGetSCXMLApplicationData = function(ors_url, callback) {
	
	var requestUrl = ors_url + '/serverx?activeApplications';
	
	send_HTTP_GET_With_JSONContent(requestUrl, {}, callback);
	
}

exports.scxmlSessionPublishEvent = scxmlSessionPublishEvent;
exports.scxmlSessionSendRequest = scxmlSessionSendRequest;
exports.scxmlSessionQuery = scxmlSessionQuery;

exports.serverGetBasicData = serverGetBasicData;
exports.serverGetConfigurationData = serverGetConfigurationData;
exports.serverGetInteractionData = serverGetInteractionData;
exports.serverGetSCXMLEngineData = serverGetSCXMLEngineData;
exports.serverGetSCXMLApplicationData = serverGetSCXMLApplicationData;
